FROM clojure:openjdk-8-lein as builder

COPY . /

RUN pwd

WORKDIR /

RUN lein uberjar

FROM openjdk:8-alpine

COPY --from=builder target/uberjar/shiver-search.jar /shiver-search/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/shiver-search/app.jar"]
