(ns shiver-search.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [shiver-search.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[shiver-search started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[shiver-search has shut down successfully]=-"))
   :middleware wrap-dev})
