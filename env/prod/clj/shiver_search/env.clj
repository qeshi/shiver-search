(ns shiver-search.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[shiver-search started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[shiver-search has shut down successfully]=-"))
   :middleware identity})
