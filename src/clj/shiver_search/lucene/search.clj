(ns shiver-search.lucene.search
  (:gen-class)
  (:require
   [msync.lucene :as lucene]
   [msync.lucene
    [document :as ld]
    ]
   [msync.lucene.analyzers :as analyzers]
   [msync.lucene.indexer :as indexer]
   [jsonista.core :as j]
   [clojure.java.io :as java-io]
   [clojure.string :refer [lower-case split]]
   [clojure.core.reducers :as r]
   [iota]
   [shiver-search.config :refer [env]]
   [mount.core :refer [defstate]  ]

   )
  )



;;(def ad-path (str (System/getProperty "user.home") "/utveckling/data/2019/60000ads.jsonl"))

                                        ;(def index-path "/tmp/ad-index/")
;;(def index-path "/tmp/ad-index-big/")
;;(def index-path "/tmp/ad-index-medium/")

(defn get-index-path []
  (:index-path env)
  )

(defn get-ad-path []
  (:ad-path env)
  )

(defn load-job-ads-from-ad-path []
  (iota/seq (get-ad-path)))




;; In the common namespace
;; This is the default analyzer, an instance of the StandardAnalyzer
;; of Lucene
(defonce default-analyzer (analyzers/standard-analyzer))

;; This analyzer considers field values verbatim
;; Will not tokenize and stem
(defonce keyword-analyzer (analyzers/keyword-analyzer))

;; laddar indexet
(defn create-ad-index-disk [] (lucene/create-index! :type :disk
                                             :path (get-index-path)
                                             :analyzer default-analyzer))


(defstate ^:dynamic *index-connection*
  :start (create-ad-index-disk)
  )


(defn parse-json-to-string [line]
  (map #(clojure.string/split (second %) #"\s+") (select-keys (j/read-value line) ["PLATSBESKRIVNING" "PLATSRUBRIK"]  ))
  )


(defn parse-ad [line]
  (let [ad (j/read-value line)]
    {:text (get-in ad ["description" "text"])
     :headline (get ad "headline")
     })
  )
;; (lucene/index! ad-index-disk [(parse-ad load-ad)] {:stored-fields [:text :headline]})

;; indexerar nya annonser
(defn index-ads-from-disk! []
  (lucene/index! *index-connection* (map parse-ad (load-job-ads-from-ad-path)) {:stored-fields [:text :headline]})
  )

(defn index-new-ads! [ads]
  (lucene/index! *index-connection* ads  {:stored-fields [:text :headline]})
  )

(defn search-ads
  "Example query-map {:text \"sjuk\" :headline \"chef\"}"
  [query-map ]
  (do
    (println query-map)

    (lucene/search *index-connection* query-map
                   {:results-per-page 10
                    :hit->doc ld/document->map
                    }))
  )

(defn search-simple [q]
  (lucene/search *index-connection* {:text q}
                 {:results-per-page 3
                  :hit->doc ld/document->map
                  })
  )

(comment
  skapa luminus project som kan indexera annonser
  Ha miljöflagga som avgör om det är en writer eller reader

  )
