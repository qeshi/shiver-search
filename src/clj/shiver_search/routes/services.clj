(ns shiver-search.routes.services
  (:require
   [reitit.swagger :as swagger]
   [reitit.swagger-ui :as swagger-ui]
   [reitit.ring.coercion :as coercion]
   [reitit.coercion.spec :as spec-coercion]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [reitit.ring.middleware.multipart :as multipart]
   [reitit.ring.middleware.parameters :as parameters]
   [shiver-search.middleware.formats :as formats]
   [shiver-search.middleware.exception :as exception]
   [ring.util.http-response :refer :all]
   [clojure.java.io :as io]
   [shiver-search.lucene.search :as search]
   [spec-tools.data-spec :as ds]
   [shiver-search.config :refer [env]]))

(defn service-routes []
  ["/api"
   {:coercion spec-coercion/coercion
    :muuntaja formats/instance
    :swagger {:id ::api}
    :middleware [;; query-params & form-params
                 parameters/parameters-middleware
                 ;; content-negotiation
                 muuntaja/format-negotiate-middleware
                 ;; encoding response body
                 muuntaja/format-response-middleware
                 ;; exception handling
                 exception/exception-middleware
                 ;; decoding request body
                 muuntaja/format-request-middleware
                 ;; coercing response bodys
                 coercion/coerce-response-middleware
                 ;; coercing request parameters
                 coercion/coerce-request-middleware
                 ;; multipart
                 multipart/multipart-middleware]}

   ;; swagger documentation
   ["" {:no-doc true
        :swagger {:info {:title "my-api"
                         :description "https://cljdoc.org/d/metosin/reitit"}}}

    ["/swagger.json"
     {:get (swagger/create-swagger-handler)}]

    ["/api-docs/*"
     {:get (swagger-ui/create-swagger-ui-handler
            {:url "/api/swagger.json"
             :config {:validator-url nil}})}]]

   ["/ping"
    {:get (constantly (ok {:message "pong"}))}]

   ["/search-simple"
    {:get {:summary "search job ads"
           :parameters {:query {:query string?}}
           :responses {200 {:body any?}}
           :handler (fn [{{{:keys [query]} :query} :parameters}]
                      {:status 200
                       :body (search/search-simple query)})}}]

   (when (= "WRITER" (:lucene-index-role env))


     ["/ad"
      {:post {:summary "index a job ad"
              :parameters {:query {:text string? :headline string?}}
              :responses {200 {:body any?}}
              :handler (fn [{{{:keys [text headline]} :query} :parameters}]
                         {:status 200
                          :body (search/index-new-ads! [{:text text :headline headline}])})}}])

   (when (= "WRITER" (:lucene-index-role env))
         ["/index-ads-from-disk"
          {:post {:summary "index a job ad"
                  :responses {200 {:body any?}}
                  :handler (fn [{{{:keys []} :query} :parameters}]
                             {:status 200
                              :body (search/index-ads-from-disk!)})}}])

   ])
